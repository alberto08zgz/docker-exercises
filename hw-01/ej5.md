Ejercicio 5
===========

version: '3.6'\
services:\
  es:\
    image: docker.elastic.co/elasticsearch/elasticsearch:7.9.3\
    container_name: es\
    environment:\
      - discovery.type=single-node\
    networks:\
      - es\
    ports:\
      - 9200:9200\
      - 9300:9300\
\
  kib:\
    image: docker.elastic.co/kibana/kibana:7.9.3\
    container_name: kib\
    networks:\
      - es\
    environment:\
      ELASTICSEARCH_HOST: es\
      ELASTICSEARCH_PORT: 9200\
    ports:\
      - 5601:5601\
\
networks:\
  es:\
    driver: bridge