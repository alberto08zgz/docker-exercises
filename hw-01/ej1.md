Ejercicio 1
===========

CMD: ejecuta un comando después de poner el contenedor en marcha. El entrypoint por defecto, que suele ser un shell, ejecuta el comando que especifiquemos con CMD
ENTRYPOINT: sirve para restringir qué queremos que interprete los comandos (parámetros incluidos) introducidos al lanzar el contenedor