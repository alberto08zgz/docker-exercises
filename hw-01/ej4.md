Ejercicio 4
===========

FROM nginx:1.19.3
HEALTHCHECK --interval=45s --timeout=5s --retries=2 --start-period=60s CMD curl --fail http://localhost:80