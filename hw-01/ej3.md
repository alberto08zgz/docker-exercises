Ejercicio 3
===========

docker pull nginx:1.19.3\
docker volume create static_content\
docker run -d -p 8080:80 --mount source=static_content,target=/usr/share/nginx/html/ nginx:1.19.3\
docker exec -it [CONTAINER_ID] /bin/bash\
echo 'HOMEWORK 1' > /usr/share/nginx/html/index.html\
exit